ALTER TABLE ONLY "raid_inscriptions"."activite_raid"
    ADD CONSTRAINT "pk_activite_raid" PRIMARY KEY ("code_raid", "code_activite");

ALTER TABLE ONLY "raid_inscriptions"."departement"
    ADD CONSTRAINT "pk_departement" PRIMARY KEY ("numero");

ALTER TABLE ONLY "raid_inscriptions"."activite"
    ADD CONSTRAINT "pk_activite" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_inscriptions"."coureur"
    ADD CONSTRAINT "pk_coureur" PRIMARY KEY ("licence");

ALTER TABLE ONLY "raid_inscriptions"."equipe"
    ADD CONSTRAINT "pk_equipe" PRIMARY KEY ("numero", "code_raid");

ALTER TABLE ONLY "raid_inscriptions"."integrer_equipe"
    ADD CONSTRAINT "pk_integrer" PRIMARY KEY ("numero_equipe", "code_raid", "licence");

ALTER TABLE ONLY "raid_inscriptions"."raid"
    ADD CONSTRAINT "pk_raid" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_inscriptions"."traverser"
    ADD CONSTRAINT "pk_traverser" PRIMARY KEY ("code_raid", "numero_dept");

ALTER TABLE ONLY "raid_inscriptions"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_activite_fkey" FOREIGN KEY ("code_activite") REFERENCES "raid_inscriptions"."activite"("code");

ALTER TABLE ONLY "raid_inscriptions"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_inscriptions"."raid"("code");

ALTER TABLE ONLY "raid_inscriptions"."equipe"
    ADD CONSTRAINT "equipe_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_inscriptions"."raid"("code");

ALTER TABLE ONLY "raid_inscriptions"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_code_coureur_fkey" FOREIGN KEY ("licence") REFERENCES "raid_inscriptions"."coureur"("licence");

ALTER TABLE ONLY "raid_inscriptions"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_equipe_fkey" FOREIGN KEY ("numero_equipe", "code_raid") REFERENCES "raid_inscriptions"."equipe"("numero", "code_raid");

ALTER TABLE ONLY "raid_inscriptions"."traverser"
    ADD CONSTRAINT "traverser_code_dept_fkey" FOREIGN KEY ("numero_dept") REFERENCES "raid_inscriptions"."departement"("numero");

ALTER TABLE ONLY "raid_inscriptions"."traverser"
    ADD CONSTRAINT "traverser_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_inscriptions"."raid"("code");
